
# Example - Bug in markdown

## Without newline
1. This code will be incorrectly rendered
```shell
git status
```
2. Same here
```shell
git status
```
3. Same here
```shell
git status
```

## With newline
1. This code will be correctly rendered
```shell
git status
```

2. Same here
```shell
git status
```
3. Same here
```shell
git status
```
